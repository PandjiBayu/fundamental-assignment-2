// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const octahedroneVol = require("./Function/octahedroneVol"); // import octahedroneVol
const sphere = require("./Function/sphere"); // import cube
const prismVol = require("./Function/prismVol"); // import prismVol

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

// Function to display the menu
function menu() {
  console.log(`Menu`);
  console.log(`====`);
  console.log(`1. Octahedrone`);
  console.log(`2. Sphere`);
  console.log(`3. Prism`);
  console.log(`4. Exit`);
  rl.question(`Choose option: `, (option) => {
    if (!isNaN(option)) {
      // If option is a number it will go here
      if (option == 1) {
        octahedroneVol.input(); // It will call input() function in octahedrone file
      } else if (option == 2) {
        sphere.inputSphere(); // It will call inputSphere() function in sphere file
      } else if (option == 3) {
        prismVol.inputBaseAreaPrism(); // It will call inputBaseAreaPrism() function in prism file
      } else if (option == 4) {
        rl.close(); // It will close the program
      } else {
        console.log(`Option must be 1 to 4!\n`);
        menu(); // If option is not 1 to 3, it will go back to the menu again
      }
    } else {
      // If option is not a number it will go here
      console.log(`Option must be number!\n`);
      menu(); // If option is not 1 to 3, it will go back to the menu again
    }
  });
}

menu(); // call the menu function to display the menu

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
module.exports.menu = menu;

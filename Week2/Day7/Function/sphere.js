const index = require("../index"); // Import index to run rl on this file

// Function to calculate sphere volume
function calculateSphereVolume(radius) {
  let r3 = radius ** 3;
  return (4 / 3) * Math.PI * r3;
}

// Function to input the length
function inputSphere() {
  index.rl.question(`Sphere radius: `, (radius) => {
    if (!isNaN(radius) && !index.isEmptyOrSpaces(radius)) {
      console.log(`Sphere's volume is ${calculateSphereVolume(radius)}\n`);
      index.menu();
    } else {
      console.log(`radius must be number`);
      inputSphere();
    }
  });
}

module.exports = { inputSphere }; // Export the input, so the another file can run this code

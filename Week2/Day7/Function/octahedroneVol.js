const index = require("../index");

// The formula to calculate octahedrone volume
function octahedroneVol(edge) {
  return (Math.sqrt(2) / 3) * edge ** 3;
}

function input() {
  index.rl.question("Edge: ", function (edge) {
    console.log(edge);
    if (!isNaN(edge)) {
      console.log(`\nOctahedrone: ${octahedroneVol(edge)}`);
      index.menu();
    } else {
      console.log(`Edge, must be a number\n`);
      input();
    }
  });
}

module.exports = { input };
